README


NIST ATS Version 1.0
Beta Test Release Notes
February, 1992


Thank you for participating in the beta testing of the NIST 
Algorithm Testing System, Version 1.0.


Please complete and return the comment sheet.  If you do not return
the comment sheet, we will assume that you do not want to participate
in the testing of the ATS.  We will then remove your name from the
distribution list.


This distribution contains the following items:

	Three Documents
		A draft of the ATS User's Guide
		NISTIR 4740 Form Error Models for the NIST ATS
		A draft report of the ATS reference algorithms
		A draft report of the production ATS requirements

	One 5.25" 1.2Mb diskette with the following files:
		README		this file
		INSTALL.BAT	installation batch program
		DISCLAIM.TXT	ATS disclaimer
		EXE.ZIP		compressed ATS executable
		SRC.ZIP		compressed ATS source code
		INCLUDE.ZIP	compressed ATS header files
		DOC.ZIP		compressed documents listed above
		EXAMPLE.ZIP	compressed example files
		DATA.ZIP	compressed PTB data files
		PKUNZIP.EXE	program for extracting compressed files


When the INSTALL script is executed (see the ATS User's Guide for 
instructions), the diskette contents are uncompressed as follows:

EXE.ZIP
	ats.exe				ATS executable

SRC.ZIP
	src\compare\cirdiff.c		compare two circle fits
	src\compare\cirsing.c		analyze a single circle fit
	src\compare\compare.c		comparator interface
	src\compare\conediff.c		compare two cone fits
	src\compare\conesing.c		analyze a single cone fit
	src\compare\cyldiff.c		compare two cylinder fits
	src\compare\cylsing.c		analyze a single cylinder fit
	src\compare\linediff.c		compare two line fits
	src\compare\linesing.c		analyze a single line fit
	src\compare\planedif.c		compare two plane fits
	src\compare\plsing.c		analyze a single plane fit
	src\compare\sphdiff.c		compare two sphere fits
	src\compare\sphsing.c		analyze a single sphere fit
	src\datamgt\collect.c		collect name lists from data manager
	src\datamgt\dsdmgt.c		manage datasets & dataset desc
	src\datamgt\fdmgt.c		manage fit description
	src\datamgt\tsdmgt.c		manage test description
	src\dataset\datapnt.c		basic data point functions
	src\dataset\dataset.c		basic dataset functions
	src\dataset\dscopy.c		copy dataset
	src\dataset\dsmean.c		compute dataset mean
	src\dataset\dsrotate.c		rotate dataset
	src\dataset\dstrans.c		translate dataset
	src\dataset\pntrot.c		rotate data point
	src\datagen\addme.c		add measurement error to a data point
	src\datagen\gencir.c		generate points on a circle	
	src\datagen\gencon.c		generate points on a cone
	src\datagen\gencyl.c		generate points on a cylinder
	src\datagen\generate.c		data generator interface
	src\datagen\genlin.c		generate points on a line
	src\datagen\genpla.c		generate points on a plane
	src\datagen\gensph.c		generate points on a sphere
	src\fit\fit.c			fitting interface
	src\fit\l2cir.c			fit a least squares circle
	src\fit\fitparms.c		transform geometry & fit parameters
	src\fit\fitutil.c		utilities for fitting functions
	src\fit\l2con.c			fit a least squares cone
	src\fit\l2cyl.c			fit a least squares cylinder
	src\fit\l2sph.c			fit a least squares sphere
	src\fit\l2lin.c			fit a least squares line
	src\fit\l2pla.c			fit a least squares plane
	src\geometry\access.c		access geometry parameters
	src\geometry\geomcvt.c		convert between geometry types
	src\geometry\geometry.c		basic geometry model functions
	src\tests\formerr.c		basic form error model functions
	src\tests\measerr.c		basic measurement error model fncs
	src\tests\sampling.c		basic sampling model functions
	src\tests\tests.c		basic test description functions
	src\math\linalg\bldaxes3.c	build 3D coordinate system
	src\math\linalg\choleski.c	perform Choleski decompostion
	src\math\linalg\dot.c		compute dot product of two vectors
	src\math\linalg\jacobi.c	compute eigenvalue & eigenvector
	src\math\linalg\mag.c		compute vector magnitude
	src\math\linalg\vecwtsum.c	compute weighted sum of two vectors
	src\math\linalg\vecdiff.c	compute vector difference
	src\math\linalg\matcopy.c	copy matrix
	src\math\linalg\matdiff.c	compute matrix difference
	src\math\linalg\matfill.c	fill matrix
	src\math\linalg\matident.c	create identity matrix
	src\math\linalg\matmoler.c	set leading square to moler matrix
	src\math\linalg\matmult.c	multiply two matrices
	src\math\linalg\matrix.c	basic matrix functions
	src\math\linalg\matscale.c	scale matrix
	src\math\linalg\matsum.c	add two matrices
	src\math\linalg\matvprod.c	multiply vector by matrix
	src\math\linalg\matxpose.c	transpose matrix
	src\math\linalg\unit.c		compute unit vector
	src\math\linalg\vecsum.c	add two vectors
	src\math\linalg\veccopy.c	copy vector
	src\math\linalg\vecfill.c	fill vector
	src\math\linalg\vecouter.c	compute outer product
	src\math\linalg\vecscale.c	scale vector
	src\math\linalg\vector.c	basic vector functions
	src\math\linalg\matsvd.c	perform matrix singular value decomp
	src\math\linalg\vecxprod.c	compute cross product of two vectors
	src\math\optimize\marq.c	perform Marquardt minimization
	src\math\stat\random.c		random number generator
	src\frontend\ats.c		main program
	src\frontend\feanal.c		manage analysis (comparator) window
	src\frontend\fecntxt.c		manage context info for user i\f
	src\frontend\feds.c		manage data (set) window
	src\frontend\fefit.c		manage fit window
	src\frontend\fefitobj.c		manage fit objective window
	src\frontend\feform.c		manage form error model (test) window
	src\frontend\fegeom.c		manage geometry model (test) window
	src\frontend\fegrinit.c		initialize display for user i\f
	src\frontend\femeas.c		manage measurement error (test) window
	src\frontend\fesamp.c		manage sampling plan (test) window
	src\frontend\fetest.c		manage test (data generator) window
	src\frontend\feutil.c		utility functions
	src\frontend\fegetfil.c		get file name from user

INCLUDE.ZIP	
	include\ats.h			basic ATS constants
	include\atscomp.h		comparator types and functions
	include\atsdata.h		dataset\point types and functions
	include\atsdgen.h		data generator types and functions
	include\atsdmgt.h		data management types and functions
	include\atsdsd.h		dataset description types and fncs
	include\atsfd.h			fit description types and functions
	include\atsfe.h			frontend (user i\f) types and fncs
	include\atstests.h		test description types and functions
	include\atsfit.h		fit types and functions
	include\atsgeom.h		geometry types and functions
	include\atsmath.h		math types and functions
	include\festuff.h		common C and C-scape headers
	include\compare.h		private comparator functions
	include\fecntxt.h		private frontend types and functions
	include\fitutil.h		private fit utilities functions
	include\datagen.h		private data generator types and fncs

DOC.ZIP
	doc\atsuserg.wp			user guide in WP format
	doc\appndxa.wp			user guide appendix A in WP format
	doc\appndxb.wp			user guide appendix B in WP format
	doc\appndxc.wp			user guide appendix C in WP format
	doc\appndxd.wp			user guide appendix D in WP format
	doc\formerr.wp			form error model document in WP format
	doc\atsalgs.wp			reference algorithm doc in WP format
	doc\prodats.wp			requirements doc for production ATS

EXAMPLE.ZIP	
	example\dataop.dsd		example of opening a dataset
	example\dataop.tsd		example of opening a dataset
	example\fitop.fd		example of opening a fit
	example\testop.tsd		example of opening a test

DATA.ZIP
	data\ptb_a\dataset.a*		PTB datasets (1-52) for round A
	data\ptb_b\dataset.b*		PTB datasets (1-44) for round B
	data\ptb_b\comment.b*		comments for PTB datasets for round B
